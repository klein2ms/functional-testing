namespace Global

// open System
// open System.Web.Http
// open System.Web.Http.Dispatcher
// open BookingApi
// open BookingApi.Validate

// type CompositionRoot() =
//   interface IHttpControllerActivator with
//     member this.Create(request, controllerDescriptor, controllerType) =
//       if controllerType = typeof<ReservationsController> then
//         let getReservations _ = 1
//         let imp =
//           Validate.reservationValid
//           >> Rop.bind (Capacity.check 10 getReservations)
//           >> Rop.map getReservations
//         new ReservationsController(imp) :> _
//       else invalidArg "controllerType" (sprintf "Unknown Controller type: %O" controllerType)

// type HttpRouteDefaults = { Controller: string; Id: obj }

// type Global() =
//   inherit System.Web.HttpApplication()
//   member this.Application_Start (sender: obj) (e: EventArgs) =
//     GlobalConfiguration.Configuration.Routes.MapHttpRoute(
//         "DefaultAPI",
//         "{controller}/{id}",
//         { Controller = "Home"; Id = RouteParameter.Optional }) |> ignore

//     GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerliazerSettings.ContractResolver <-
//       Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver()

//     GlobalConfiguration.Configuration.Services.Replace(
//       typeof<IHttpControllerActivator>,
//       CompositionRoot())