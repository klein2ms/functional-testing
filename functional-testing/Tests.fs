module Tests

open System
open Expecto
open Swensen.Unquote

let convertsTo<'a> candidate =
  match box candidate with
  | :? 'a as converted -> Some converted
  | _ -> None

module Messaging =
  type Envelope<'a> = {
    Id : Guid
    Created : DateTimeOffset
    Item : 'a }

  let envelop getId getTime item = {
    Id = getId ()
    Created = getTime ()
    Item = item }

  let getMachineTime () = DateTimeOffset.Now

type Foo = { Text: string; Number: int }

[<Tests>]
let tests =
  testList "samples" [
    testCase "envelop returns the correct result" <| fun _ ->
      let getId _ = Guid "06243B7E-820C-4046-AD2F-65A2B04E027C"
      let getTime _ = DateTimeOffset(636480992531657038L, TimeSpan.FromHours 1.)
      let item = { Text = "Bar"; Number = 42 }

      let actual = Messaging.envelop getId getTime item

      let expected : Messaging.Envelope<Foo> = {
        Id = getId ()
        Created = getTime ()
        Item = item }

      test <@ actual = expected @>

    testCase "getMachineTime returns the correct result" <| fun _ ->
      let before = DateTimeOffset.Now

      let actual = Messaging.getMachineTime ()

      let after = DateTimeOffset.Now

      test <@ before <= actual && actual <= after @>

    testList "numberology 101" (
      testParam 1333
        [ "First sample", fun value () ->  test <@ value = 1333 @>
          "Second sample", fun value () -> test <@ value < 1444 @> ]
      |> List.ofSeq)
  ]