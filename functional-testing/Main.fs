module TestRunner

open Expecto

[<Tests>]
let tests =
  testList "Tests" [
    Tests.tests
    DiamondProperties.tests
    ApiTests.ControlerTests.tests
    ApiTests.ValidateTests.tests
    ApiTests.CapacityTests.tests
  ]

[<EntryPoint>]
let main args = runTestsWithArgs defaultConfig args tests