namespace ApiTests

open BookingApi
open Expecto
open FsCheck
open Rop
open System

module ValidateTests =

  [<Tests>]
  let tests =
    testList "Validate.reservationValid" [
      testProperty "returns correct result given valid date"
        <| fun (rendition: ReservationRendition) (date: DateTimeOffset) ->

        let rendition = { rendition with Date = date.ToString "o" }

        let actual = Validate.reservationValid rendition

        let expected = Success {
          Date = date
          Name = rendition.Name
          Email = rendition.Email
          Quantity = rendition.Quantity }

        actual = expected

      testProperty "returns correct result given invalid date"
        <| fun (rendition: ReservationRendition) ->
        not (fst(DateTimeOffset.TryParse rendition.Date)) ==> lazy

        let actual = Validate.reservationValid rendition

        let expected : Result<Reservation, Error> =
          Failure (ValidationError("Invalid date."))

        actual = expected
    ]

module CapacityTests =

  [<Tests>]
  let tests =
    testList "Capacity.check" [
      testProperty "returns correct result when no reservations"
        <| fun (reservation: Reservation) (excessCapacity : int) ->
        (reservation.Quantity >= 0 && excessCapacity >= 0) ==> lazy

        let capacity = reservation.Quantity + excessCapacity
        let getReservedSeats _ = 0

        let actual =
          Capacity.check
            capacity
            getReservedSeats
            reservation

        let expected : Result<Reservation, Error> =
          Success reservation

        actual = expected

      testProperty "returns correct result when no capacity"
        <| fun (reservation : Reservation) (capacity : int) (reservedSeats : int) ->
        (capacity > 0
          && reservedSeats >= 0
          && capacity < reservedSeats + reservation.Quantity) ==> lazy

        let getReservedSeats _ = reservedSeats

        let actual =
          Capacity.check
            capacity
            getReservedSeats
            reservation

        let expected : Result<Reservation, Error> =
          Failure CapacityExceeded

        actual = expected
    ]

module ControlerTests =

  open System.Net
  open System.Web.Http

  [<Tests>]
  let tests =
    testList "ReservationsController" [
      testProperty "POST returns correct result on Success"
        <| fun (rendition: ReservationRendition) ->

        let imp _ = Success ()
        use sut = new ReservationsController(imp)

        sut.Post rendition
        :? Results.OkResult

      testProperty "POST returns correct result on validation error"
        <| fun (rendition: ReservationRendition) ->

        let imp _ = Failure(ValidationError "Invalid date.")
        use sut = new ReservationsController(imp)

        sut.Post rendition
        :? Results.BadRequestErrorMessageResult

      testProperty "POST returns correct result on capacity exceeded"
        <| fun (rendition: ReservationRendition) ->

        let imp _ = Failure CapacityExceeded
        use sut = new ReservationsController(imp)

        sut.Post rendition
        |> Tests.convertsTo<Results.StatusCodeResult>
        |> Option.map (fun x -> x.StatusCode)
        |> Option.exists ((=) HttpStatusCode.Forbidden)
    ]
