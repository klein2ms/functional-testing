module DiamondProperties

open Diamond
open Expecto
open FsCheck
open System

type Letters =
    static member Char() =
        Arb.Default.Char()
        |> Arb.filter (fun c -> 'A' <= c && c <= 'Z')

let config = { FsCheckConfig.defaultConfig with arbitrary = [ typeof<Letters>; ] }

[<Tests>]
let tests =
  testList "Diamond Properties" [
    testPropertyWithConfig config "Diamond is non-empty" <| fun (letter: char) ->
      make letter
      |> String.IsNullOrWhiteSpace
      |> not

    testPropertyWithConfig config "First row contains 'A'" <| fun (letter: char) ->
      make letter
      |> split
      |> Seq.head
      |> trim = "A"

    testPropertyWithConfig config "All rows must have symmetric contour" <| fun (letter: char) ->
      make letter
      |> split
      |> Array.forall (fun r -> leadingSpaces r = trailingSpaces r)

    testPropertyWithConfig config "Top of figure has correct letters in correct order" <| fun (letter: char) ->
      let expected = [ 'A' .. letter ]

      make letter
      |> split
      |> Seq.take expected.Length
      |> Seq.map trim
      |> Seq.map Seq.head
      |> Seq.toList
      |> ((=)) expected

    testPropertyWithConfig config "Figure is symmetric around the horizontal axis" <| fun (letter: char) ->
      let actual = make letter
      let rows = split actual
      let topRows =
          rows
          |> Seq.takeWhile (fun x -> not (x.Contains(string letter)))
          |> Seq.toList
      let bottomRows =
          rows
          |> Seq.skipWhile (fun x -> not (x.Contains(string letter)))
          |> Seq.skip 1
          |> Seq.toList
          |> List.rev
      topRows = bottomRows

    testPropertyWithConfig config "Diamond is as wide as it is high" <| fun (letter: char) ->
      let actual = make letter

      let rows = split actual
      let expected = rows.Length
      rows |> Array.forall (fun x -> x.Length = expected)

    testPropertyWithConfig config "All rows except top and bottom have two identical letters" <| fun (letter: char) ->
      let actual = make letter

      let isTwoIdenticalLetters x =
          let hasIdenticalLetters = x |> Seq.distinct |> Seq.length = 1
          let hasTwoLetters = x |> Seq.length = 2
          hasIdenticalLetters && hasTwoLetters

      let rows = split actual
      rows
      |> Array.filter (fun x -> not (x.Contains("A")))
      |> Array.map (fun x -> x.Replace(" ", ""))
      |> Array.forall isTwoIdenticalLetters

    testPropertyWithConfig config "Lower left space is a triangle" <| fun (letter: char) ->
      let actual = make letter

      let rows = split actual
      let lowerLeftSpace =
          rows
          |> Seq.skipWhile (fun x -> not (x.Contains(string letter)))
          |> Seq.map leadingSpaces
          |> Seq.toList

      let spaceCounts = lowerLeftSpace |> List.map (fun x -> x.Length)
      let expected =
          Seq.initInfinite id
          |> Seq.take spaceCounts.Length
          |> Seq.toList
      expected = spaceCounts
]
